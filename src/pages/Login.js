import { useContext, useEffect, useState } from "react";
import { Button, Form, Row, Col, Container } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import { UserContext } from "../store/UserContext";
import Swal from "sweetalert2";

export default function Login() {
    const userContext = useContext(UserContext);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        if (email && password) return setIsActive(true);
        setIsActive(false);
    }, [email, password]);

    const submitHandler = async (event) => {
        event.preventDefault();

        const userCredentials = {
            email: email,
            password: password,
        };
        //
        // PASS to separate JS that handles fetch requests
        //
        const response = await fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(userCredentials),
        });
        const userToken = await response.json();

        const userDetails = async (token) => {
            const response = await fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
                headers: { Authorization: `Bearer ${token}` },
            });
            const userData = await response.json();
            userContext.setUser({ id: userData._id, isAdmin: userData.isAdmin });
        };

        if (userToken.accessToken !== "empty") {
            localStorage.setItem("token", userToken.accessToken);
            userDetails(userToken.accessToken);
            Swal.fire({
                title: "Login Successful!",
                icon: "success",
                text: "Naka-login ka na!",
            });
        } else {
            Swal.fire({
                title: "Login Failed!",
                icon: "error",
                text: "Check Your Login Details and Try Again.",
            });
            setPassword("");
        }
    };

    return userContext.user != null ? (
        <Navigate to={"/home"} />
    ) : (
        <Container>
            <Row>
                <Col className="col-4 offset-4 bg-white shadow p-3 mt-3">
                    <Form onSubmit={submitHandler}>
                        <Form.Group className="mb-3" controlId="email">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="Enter email"
                                value={email}
                                onChange={(event) => {
                                    setEmail(event.target.value);
                                }}
                                required
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Password"
                                value={password}
                                onChange={(event) => {
                                    setPassword(event.target.value);
                                }}
                                required
                            />
                        </Form.Group>
                        <Button variant="primary" type="submit" disabled={!isActive}>
                            Login
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
}
