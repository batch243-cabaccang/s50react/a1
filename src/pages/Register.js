import { useContext, useEffect, useState } from "react";
import { Button, Form, Row, Col, Container } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import { UserContext } from "../store/UserContext";

export default function Register() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [isActive, setIsActive] = useState(false);
    const [passwordConfirmed, setPasswordConfirmed] = useState(false);
    const [isValidMobileNo, setIsValidMobileNo] = useState(false);
    const userContext = useContext(UserContext);

    useEffect(() => {
        if (password === confirmPassword) return setPasswordConfirmed(true);
        setPasswordConfirmed(false);
    }, [password, confirmPassword]);

    useEffect(() => {
        if (mobileNo.length < 11) return setIsValidMobileNo(false);
        setIsValidMobileNo(true);
    }, [mobileNo]);

    useEffect(() => {
        if (email && password && passwordConfirmed && firstName && lastName && isValidMobileNo)
            return setIsActive(true);
        setIsActive(false);
    }, [email, password, passwordConfirmed, firstName, lastName, isValidMobileNo]);

    const submitHandler = async (event) => {
        event.preventDefault();

        const userCredentials = {
            email: email,
            password: password,
            firstName: firstName,
            lastName: lastName,
            mobileNo: mobileNo,
        };
        //
        // PASS to separate JS that handles fetch requests
        //
        const response = await fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(userCredentials),
        });
        const newUser = await response.json();

        if (newUser === `Congratulations, Sir/Ma'am ${firstName}!. You are now registered.`) {
            const response = await fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    email: userCredentials.email,
                    password: userCredentials.password,
                }),
            });
            const userToken = await response.json();

            const userDetails = async (token) => {
                const response = await fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
                    headers: { Authorization: `Bearer ${token}` },
                });
                const userData = await response.json();
                userContext.setUser({ id: userData._id, isAdmin: userData.isAdmin });
            };

            if (userToken.accessToken !== "empty") {
                localStorage.setItem("token", userToken.accessToken);
                userDetails(userToken.accessToken);
                Swal.fire({
                    title: "You are now registered!",
                    icon: "success",
                    text: "Welcome to the club!",
                });
            }
            setEmail("");
            setPassword("");
            setConfirmPassword("");
            setFirstName("");
            setLastName("");
            setMobileNo("");
        } else {
            Swal.fire({
                title: "Something went wrong!",
                icon: "error",
                text: "Please Try Again.",
            });
        }
    };

    return userContext.user ? (
        <Navigate to="/home" />
    ) : (
        <Container>
            <Row>
                <Col className="col-4 offset-4 bg-white shadow p-3 mt-3">
                    <Form onSubmit={submitHandler}>
                        <Form.Group className="mb-3" controlId="email">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="Enter email"
                                value={email}
                                onChange={(event) => {
                                    setEmail(event.target.value);
                                }}
                                required
                            />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Password"
                                value={password}
                                onChange={(event) => {
                                    setPassword(event.target.value);
                                }}
                                required
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="retypepassword">
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Re-Type Password"
                                value={confirmPassword}
                                onChange={(event) => {
                                    setConfirmPassword(event.target.value);
                                }}
                                required
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="firstName">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="First Name"
                                value={firstName}
                                onChange={(event) => {
                                    setFirstName(event.target.value);
                                }}
                                required
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="lastName">
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Last Name"
                                value={lastName}
                                onChange={(event) => {
                                    setLastName(event.target.value);
                                }}
                                required
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="mobileNo">
                            <Form.Label>Mobile No</Form.Label>
                            <Form.Control
                                type="number"
                                placeholder="Mobile No."
                                value={mobileNo}
                                onChange={(event) => {
                                    setMobileNo(event.target.value);
                                }}
                                required
                            />
                            {!isValidMobileNo && (
                                <Form.Text>Please Enter At Least 11 digits.</Form.Text>
                            )}
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicCheckbox">
                            <Form.Check type="checkbox" label="Check me out" />
                        </Form.Group>
                        <Button variant="primary" type="submit" disabled={!isActive}>
                            Submit
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
}
