import { useContext } from "react";
import { Navigate } from "react-router-dom";
import { UserContext } from "../store/UserContext";

export default function Logout() {
    const userContext = useContext(UserContext);
    userContext.logoutHandler();
    return <Navigate to="/login" />;
}
