import { Fragment } from "react";
import { Link } from "react-router-dom";

export default function PageNotFound() {
    return (
        <Fragment>
            <h2>Page Not Found</h2>
            {"Go back to the "}
            <Link to="home">Home Page</Link>
        </Fragment>
    );
}
