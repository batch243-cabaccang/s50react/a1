import coursesData from "../data/courses.js";
import { Row, Col, Container } from "react-bootstrap";
import CourseCard from "../components/CourseCard";
import { useEffect, useState } from "react";

export default function Courses() {
    const [courses, setCourses] = useState([]);

    useEffect(() => {
        getActiveCourses();
    }, []);

    const getActiveCourses = async () => {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/courses/allActiveCourses`);
        const courses = await response.json();
        setCourses(courses);
    };

    return (
        <Container>
            <Row>
                {courses.map((course) => {
                    return (
                        <Col xs={12} md={4} key={course._id} className="offset-xs-12 offset-0">
                            <CourseCard
                                key={course._id}
                                id={course._id}
                                name={course.name}
                                description={course.description}
                                price={course.price}
                                onOffer={course.onOffer}
                                slots={course.slots}
                            />
                        </Col>
                    );
                })}
            </Row>
        </Container>
    );
}
