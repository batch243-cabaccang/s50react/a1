import { useEffect, useState } from "react";
import { Row, Col, Container, Card, Button } from "react-bootstrap";
import { useParams, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function CourseView() {
    const [name, setName] = useState();
    const [description, setDescription] = useState();
    const [price, setPrice] = useState();
    const { id } = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        getCourse();
    }, [id]);

    const getCourse = async () => {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/courses/${id}`);
        const course = await response.json();
        setName(course.name);
        setDescription(course.description);
        setPrice(course.price);
    };

    const enroll = async (courseId) => {
        const token = localStorage.getItem("token");
        const response = await fetch(`${process.env.REACT_APP_API_URL}/users/enroll/${courseId}`, {
            method: "POST",
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json",
            },
        });
        const apiResponse = await response.json();
        if (apiResponse === true) {
            Swal.fire({
                title: "Enrolled Ka Na!",
                icon: "success",
                text: "Wag Sayanging Pera, Ha?",
            });
            navigate("/courses");
        } else {
            Swal.fire({
                title: "Oops. May Error.",
                icon: "error",
                text: "Baka Admin Ka?",
            });
            navigate("/home");
        }
    };

    return (
        <Container>
            <Row>
                <Col>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {price}</Card.Text>
                            <Card.Subtitle>Class Schedule</Card.Subtitle>
                            <Card.Text>8 am - 5 pm</Card.Text>
                            <Button
                                variant="primary"
                                block
                                onClick={() => {
                                    enroll(id);
                                }}
                            >
                                Enroll
                            </Button>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}
