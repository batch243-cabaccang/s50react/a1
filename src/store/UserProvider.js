import { useEffect, useState } from "react";
import { UserContext } from "./UserContext";
import Swal from "sweetalert2";

export default function UserProvider(props) {
    // const email = localStorage.getItem("email");
    const [user, setUser] = useState(null);

    useEffect(() => {
        // userDetails(localStorage.getItem("token"));
        setUser(localStorage.getItem("token"));
    }, []);

    // const userDetails = async (token) => {
    //     const response = await fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
    //         headers: { Authorization: `Bearer ${token}` },
    //     });
    //     const userData = await response.json();
    //     userContext.setUser({ id: userData._id, isAdmin: userData.isAdmin });
    // };

    const logoutHandler = () => {
        localStorage.removeItem("token");
        setUser(null);
        Swal.fire({
            title: "You Have Logged Out.",
            icon: "info",
            text: "You'll be back. We know.",
        });
    };

    const userContext = {
        user: user,
        setUser: setUser,
        logoutHandler: logoutHandler,
    };
    return <UserContext.Provider value={userContext}>{props.children}</UserContext.Provider>;
}
