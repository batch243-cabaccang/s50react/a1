import { BrowserRouter as Routest, Routes as Router, Route, Navigate } from "react-router-dom";

import "./App.css";
import MainNavbar from "./components/MainNavbar";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Courses from "./pages/Courses";
import Logout from "./pages/Logout";
import PageNotFound from "./pages/PageNotFound";
import UserProvider from "./store/UserProvider";
import { useContext } from "react";
import { UserContext } from "./store/UserContext";
import CourseView from "./pages/CourseView";

function App() {
    const userContext = useContext(UserContext);

    return (
        <UserProvider>
            <Routest>
                <MainNavbar />
                <Router>
                    <Route path="*" element={<PageNotFound />} />
                    <Route path="/home" element={<Home />} />
                    <Route
                        path="/registration"
                        element={userContext.user ? <Navigate to="*" /> : <Register />}
                    />
                    <Route
                        path="/login"
                        element={userContext.user ? <Navigate to="*" /> : <Login />}
                    />
                    <Route path="/courses" element={<Courses />} />
                    <Route path="/logout" element={<Logout />} />
                    <Route path="/courseView/:id" element={<CourseView />} />
                </Router>
            </Routest>
        </UserProvider>
    );
}

export default App;
