import { useContext, useEffect, useState } from "react";
import { Button, Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import { UserContext } from "../store/UserContext";

export default function CourseCard(props) {
    const userContext = useContext(UserContext);
    const [slots, setSlots] = useState(props.slots);
    const [enrollees, setEnrollees] = useState(0);
    const [isOutOfSlots, setIsOutOfSlots] = useState(false);

    useEffect(() => {
        if (slots === 0) {
            setIsOutOfSlots(true);
            alert("You Got The Last Slot! No Slots Left!");
            return;
        }
    }, [slots]);

    const enrollHandler = () => {
        setSlots(slots - 1);
        setEnrollees(enrollees + 1);
    };

    return (
        <Card className="cardHighlight p-3">
            <Card.Body>
                <Card.Title>
                    <h2>{props.name}</h2>
                </Card.Title>
                <Card.Text>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>{props.description}</Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>{props.price}</Card.Text>
                    <Card.Subtitle>Enrollees:</Card.Subtitle>
                    <Card.Text>{enrollees}</Card.Text>
                    <Card.Subtitle>Slots Available:</Card.Subtitle>
                    <Card.Text>{slots}</Card.Text>
                </Card.Text>
                {userContext.user ? (
                    <Button as={Link} to={`/courseView/${props.id}`} disabled={isOutOfSlots}>
                        Details
                    </Button>
                ) : (
                    <Button as={Link} to="/login">
                        Enroll
                    </Button>
                )}
            </Card.Body>
        </Card>
    );
}
