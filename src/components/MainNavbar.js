import { Fragment, useContext, useEffect } from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { UserContext } from "../store/UserContext";

export default function MainNavbar() {
    const userContext = useContext(UserContext);

    return (
        <Navbar bg="light" expand="lg" className="vw-100">
            <Container fluid>
                <Navbar.Brand href="#home">Course Booking</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        <Nav.Link as={NavLink} to="/home">
                            Home
                        </Nav.Link>
                        <Nav.Link as={NavLink} to="/courses">
                            Courses
                        </Nav.Link>
                        {userContext.user ? (
                            <Nav.Link
                                as={NavLink}
                                to="/login"
                                onClick={() => {
                                    userContext.logoutHandler();
                                }}
                            >
                                Logout
                            </Nav.Link>
                        ) : (
                            <Fragment>
                                <Nav.Link as={NavLink} to="/login">
                                    Login
                                </Nav.Link>
                                <Nav.Link as={NavLink} to="/registration">
                                    Register
                                </Nav.Link>
                            </Fragment>
                        )}
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}
