import { Row, Col, Button, Card } from "react-bootstrap";

export default function Highlights() {
    return (
        <Row className="my-3">
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Learn From Home</h2>
                        </Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Aliquam turpis justo, convallis sollicitudin
                            mauris quis, luctus tincidunt lorem. Pellentesque
                            vitae congue elit. Pellentesque ac condimentum
                            tellus. Fusce vel dui neque. Proin non euismod
                            libero. Nulla feugiat ultricies sagittis. Nam vel
                            elit fringilla, placerat massa nec, pulvinar diam.
                            Morbi luctus odio id varius vehicula. Quisque
                            tristique, lorem ac fermentum congue, ipsum ante
                            faucibus sapien, sed aliquam tellus nibh vitae
                            velit.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Study Now, Pay Later</h2>
                        </Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Aliquam turpis justo, convallis sollicitudin
                            mauris quis, luctus tincidunt lorem. Pellentesque
                            vitae congue elit. Pellentesque ac condimentum
                            tellus. Fusce vel dui neque. Proin non euismod
                            libero. Nulla feugiat ultricies sagittis. Nam vel
                            elit fringilla, placerat massa nec, pulvinar diam.
                            Morbi luctus odio id varius vehicula. Quisque
                            tristique, lorem ac fermentum congue, ipsum ante
                            faucibus sapien, sed aliquam tellus nibh vitae
                            velit.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Be Part Of Our Community</h2>
                        </Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Aliquam turpis justo, convallis sollicitudin
                            mauris quis, luctus tincidunt lorem. Pellentesque
                            vitae congue elit. Pellentesque ac condimentum
                            tellus. Fusce vel dui neque. Proin non euismod
                            libero. Nulla feugiat ultricies sagittis. Nam vel
                            elit fringilla, placerat massa nec, pulvinar diam.
                            Morbi luctus odio id varius vehicula. Quisque
                            tristique, lorem ac fermentum congue, ipsum ante
                            faucibus sapien, sed aliquam tellus nibh vitae
                            velit.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    );
}
